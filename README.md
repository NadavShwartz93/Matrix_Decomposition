# Matrix_Decomposition

## `Program Description:`


> ### ***1. SVD :*** 
Calculation of Singular Value Decomposition (**SVD**) of Matrix -\
calculat Eigenvalues and Eigenvectors of matrix by [**Jacobi Method**](https://en.wikipedia.org/wiki/Jacobi_eigenvalue_algorithm).\
And then, calculat the <img src="https://render.githubusercontent.com/render/math?math=U, S, V^T"> of the given matrix.

![SVD.png](images/SVD.png "SVD")

> ### ***2. QR Decomposition - Using Gram–Schmidt process:***
Perform Gram–Schmidt process on the <img src="https://render.githubusercontent.com/render/math?math=N x N"> matrix and get matrix called <img src="https://render.githubusercontent.com/render/math?math=U">.\
Create <img src="https://render.githubusercontent.com/render/math?math=Q"> matrix by normalize the columns of <img src="https://render.githubusercontent.com/render/math?math=U">.\
Create <img src="https://render.githubusercontent.com/render/math?math=R"> matrix by execute <img src="https://render.githubusercontent.com/render/math?math=Q^T*A"> which is the dot product between <img src="https://render.githubusercontent.com/render/math?math=Q^T"> matrix and input matrix called <img src="https://render.githubusercontent.com/render/math?math=A">.

![QR_Decomposition.png](images/QR_Decomposition.png "QR Decomposition")

## `Project Code:`

Implementing of the project has been done in C++ and Python :

> ### C++ : 
The Class Diagram of the C++ project is located [Here](Project1/README.md). \
Project application : [Matrix_Decomposition.exe](Project1/Matrix_Decomposition.exe).\
C++ Source Code and Header Files is located [Here](https://gitlab.com/NadavShwartz93/Matrix_Decomposition/-/tree/main/Project1).\
C++ Project main files are :\
[Matrix_Operations.cpp](https://gitlab.com/NadavShwartz93/Matrix_Decomposition/-/blob/main/Project1/Source/Matrix_Operations.cpp) - contain various methods for all the required operations that can been done on matrices.\
[Jacobi.cpp](https://gitlab.com/NadavShwartz93/Matrix_Decomposition/-/blob/main/Project1/Source/Jacobi.cpp) - contain methods for [Jacobi eigenvalue algorithm](https://en.wikipedia.org/wiki/Jacobi_eigenvalue_algorithm) and methods for calculating <img src="https://render.githubusercontent.com/render/math?math=U, S, V^T"> matrices.\
[QR_Decomposition.cpp](https://gitlab.com/NadavShwartz93/Matrix_Decomposition/-/blob/main/Project1/Source/QR_Decomposition.cpp) - contain methods for calculating QR decomposition.\
[Project_main.cpp](https://gitlab.com/NadavShwartz93/Matrix_Decomposition/-/blob/main/Project1/Source/Project_main.cpp) - the main method.